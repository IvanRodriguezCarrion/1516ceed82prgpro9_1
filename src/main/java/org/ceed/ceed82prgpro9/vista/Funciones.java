package org.ceed.ceed82prgpro9.vista;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Funciones {
    
    
    public static void mensaje(JFrame j, String mensaje, String encabezado, int tipo)  {
        JOptionPane.showMessageDialog(j, mensaje, encabezado, tipo);
    }
    
}
