package org.ceed.ceed82prgpro9.controlador;
import org.ceed.ceed82prgpro9.vista.VistaGrafica;
import org.ceed.ceed82prgpro9.vista.VistaGraficaPadrino;
import org.ceed.ceed82prgpro9.vista.VistaGraficaPerro;
import org.ceed.ceed82prgpro9.vista.VistaGraficaApadrina;
import org.ceed.ceed82prgpro9.vista.VistaGraficaAcercaDe;
import org.ceed.ceed82prgpro9.modelo.IModelo;
import org.ceed.ceed82prgpro9.modelo.ModeloMysql;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import javax.swing.event.MenuListener;
import javax.swing.event.MenuEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Desktop;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.InputMismatchException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon; // Permite insertar iconos.
import org.ceed.ceed82prgpro9.modelo.ModeloDb4o;
import org.ceed.ceed82prgpro9.vista.Funciones;



/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class ControladorPrincipal implements ActionListener {
    VistaGrafica vistagrafica = new VistaGrafica();
    IModelo imodelo;
    
  public ControladorPrincipal (IModelo m, VistaGrafica v) throws InputMismatchException {
//    vistagrafica = v;
    imodelo = m;
    vistagrafica.menuGrafico();
//     Vigilamos los eventos sobre los botones que llevarán a distintas partes del menú
    vistagrafica.getBotonPadrino().addActionListener(this);
    vistagrafica.getBotonPerro().addActionListener(this);
    vistagrafica.getBotonApadrinamiento().addActionListener(this);
    vistagrafica.getBotonBdInst().addActionListener(this);
    vistagrafica.getBotonBdTab().addActionListener(this);
    vistagrafica.getBotonBdInsert().addActionListener(this);
    vistagrafica.getBotonDocuPDF().addActionListener(this);
    vistagrafica.getBotonDocuOnl().addActionListener(this);
    vistagrafica.getBotonSalir().addActionListener(this);
    vistagrafica.getBotonAcercade().addActionListener(this);  
  
    }
  
  @Override
  public void actionPerformed (ActionEvent event) { // Metodos implementados por ActionListener que vigila los botones
      if (vistagrafica.getBotonPadrino() == event.getSource()) {
          VistaGraficaPadrino menuAdmPadrino = VistaGraficaPadrino.getInstancia();
          if (!menuAdmPadrino.isVisible()) {
               vistagrafica.getEscritorio().add(menuAdmPadrino);
               menuAdmPadrino.setVisible(true);
          }
          try {
//              menuAdmPadrino.setMaximum(true);
              menuAdmPadrino.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorPadrino cp = new ControladorPadrino (imodelo, menuAdmPadrino );
      }
      else if (vistagrafica.getBotonPerro() == event.getSource()) {
          VistaGraficaPerro menuAdmPerro = VistaGraficaPerro.getInstancia();
          if (!menuAdmPerro.isVisible()) {
               vistagrafica.getEscritorio().add(menuAdmPerro);
               menuAdmPerro.setVisible(true);
          }
          try {
              menuAdmPerro.setMaximum(true);
              menuAdmPerro.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorPerro cp = new ControladorPerro (imodelo, menuAdmPerro );

      }
      else if (vistagrafica.getBotonApadrinamiento() == event.getSource()) {
          VistaGraficaApadrina menuAdmApa = VistaGraficaApadrina.getInstancia();
          if (!menuAdmApa.isVisible()) {
               vistagrafica.getEscritorio().add(menuAdmApa);
               menuAdmApa.setVisible(true);
          }
          try {
              menuAdmApa.setMaximum(true);
              menuAdmApa.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorApadrina cp = new ControladorApadrina (imodelo, menuAdmApa);
      }
      else if (vistagrafica.getBotonAcercade() == event.getSource()) {
//          VistaGraficaAcercaDe vad = new VistaGraficaAcercaDe();
          VistaGraficaAcercaDe vade = VistaGraficaAcercaDe.getInstancia();
          vade.pack();
          if (!vade.isVisible()) {
               vistagrafica.getEscritorio().add(vade);
               vade.setVisible(true);
          }
          try {
              vade.setMaximum(true);
              vade.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorAcercaDe cp = new ControladorAcercaDe (vade );
          
      }
      else if (vistagrafica.getBotonBdInst() == event.getSource()) {
//          ModeloMysql mys = new ModeloMysql();
          imodelo.instalarDb();
          Funciones.mensaje(null, "Base de datos OO creada con éxito.", "CONEXIÓN",1);
      }
      else if (vistagrafica.getBotonBdTab() == event.getSource()) {
//          my.crearTablas();
      }
      else if (vistagrafica.getBotonBdInsert() == event.getSource()) {
//          my.insertarDatos();
           imodelo.desinstalarDb();
           Funciones.mensaje(null, "Archivo DB4O (BDOO) borrado con éxito", "BORRADO",0);
      }
      
      else if (vistagrafica.getBotonDocuOnl() == event.getSource()) {
        String url = "https://docs.google.com/document/d/1MK6IUu3PphsD_4RWTs3LKS93c7YlQ4VOfGD4eRKKWqE/edit?ts=5624d84f#";
        Desktop desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.getDesktop().browse(new URI(url));
            } catch (URISyntaxException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
      }
      else if (vistagrafica.getBotonDocuPDF()== event.getSource()){
           try
    {
      String ruta = "src/main/java/org/ceed/documentacion/documentacion.pdf";
      File pdf = new File(ruta);
      Desktop.getDesktop().open(pdf);
    }
    catch (IOException ex)
    {
      Funciones.mensaje(null, "No se ha podido abrir el PDF.", "Error", 0);
    }
      }
      else if (vistagrafica.getBotonSalir() == event.getSource()) {
          System.exit(0);
      }
      
  }
   
 }
 
 

