package org.ceed.ceed82prgpro9.modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Date;


public class ModeloFichero implements IModelo {
    
    public static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    
    File archivo_padrino;
    File archivo_perro;
    File archivo_apadrinamiento;

    public ModeloFichero() {
        this.archivo_apadrinamiento = new File("apadrinamiento.csv");
        this.archivo_perro = new File("perro.csv");
        this.archivo_padrino = new File("padrino.csv");
        try {
            FileWriter fw = new FileWriter(archivo_padrino, true);
            fw.close();
           } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
         try {
            FileWriter fw = new FileWriter(archivo_perro, true);
            fw.close();
           } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
           }
         try {
            FileWriter fw = new FileWriter(archivo_apadrinamiento, true);
            fw.close();
           } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void create(Padrino p) {
    
        try {
            FileWriter fw = new FileWriter(archivo_padrino, true);
            fw.write(p.getIdPersona() + ";" + p.getNombre() + ";"+ p.getTelefono() + ";" + p.getEmail() + ";\r\n");
            fw.close();
           } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    @Override
    public void create(Perro perro) {
    
        try {
            FileWriter fw = new FileWriter(archivo_perro, true);
            fw.write(perro.getIdPerro() + ";" + perro.getNombre() + ";"+ perro.getNChip() + ";" + perro.getRaza() + ";\r\n");
            fw.close();
           } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }  
        
    }
    
    @Override
    public void create(Apadrina apa) {
    
        try {
            FileWriter fw = new FileWriter(archivo_apadrinamiento, true);
            
//            fw.write(apa.getIdApadrina() + ";" + apa.getPadrino().getIdPersona() + ";" + apa.getPadrino().getNombre() + ";"+ apa.getPadrino().getTelefono() 
//                    + ";" + apa.getPadrino().getEmail() + ";" + apa.getPerro().getIdPerro() + ";" + apa.getPerro().getNombre()
//                    + ";"+ apa.getPerro().getNChip() + ";" + apa.getPerro().getRaza() + ";\r\n");
//            
                       fw.write(apa.getIdApadrina() + ";" + apa.getIdPersona() + ";" + apa.getIdPerro() + ";" + SHORT_DATE_FORMAT.format(apa.getFecha())+ ";\r\n");
 
            fw.close();
           } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
   public ArrayList<Padrino> readPadrino() { 
        
        ArrayList hs = new ArrayList();
        
        try {
            FileReader fr = new FileReader(archivo_padrino);
            BufferedReader br = new BufferedReader(fr);
            String s;
            s = br.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String idpersona_archivo = st.nextToken();
                String nombre_padrino = st.nextToken();
                String telefono_padrino = st.nextToken();
                String email_padrino = st.nextToken();
                Padrino padrino = new Padrino(idpersona_archivo, nombre_padrino, telefono_padrino, email_padrino);
                hs.add(padrino);
                s = br.readLine();
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hs;
    }
 
    @Override
    public ArrayList<Perro> readPerro() {
        
        ArrayList hs = new ArrayList();
        
        try {
            FileReader fr = new FileReader(archivo_perro);
            BufferedReader br = new BufferedReader(fr);
            String s;
            s = br.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String idperro_archivo = st.nextToken();
                String nombre_perro = st.nextToken();
                String nChip_perro = st.nextToken();
                String raza_perro = st.nextToken();
                Perro perro = new Perro(idperro_archivo, nombre_perro, nChip_perro, raza_perro);
                hs.add(perro);
                s = br.readLine();
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hs;
    }
     
      @Override
     public ArrayList<Apadrina> readApadrina() { //Hace lo mismo que Padrino
        
        ArrayList hs = new ArrayList();
        
        try {
            FileReader filereader = new FileReader(archivo_apadrinamiento);
            BufferedReader br = new BufferedReader(filereader);
            String s;
            s = br.readLine();
            while (s != null) {                  
                StringTokenizer st = new StringTokenizer(s, ";");
                String idapadrinamiento = st.nextToken();
                String idpersona_archivo = st.nextToken();
                String idperro_archivo = st.nextToken();
                Date fecha = null;
                try
                {
                 fecha = SHORT_DATE_FORMAT.parse(st.nextToken());                
                }
                catch (ParseException pe)
                {                    
                }

                Apadrina apadrina = new Apadrina (idapadrinamiento, idpersona_archivo, idperro_archivo, fecha);
                hs.add(apadrina);
                s = br.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hs;
    }

    @Override
    public void update(Padrino p) {
        File archivo_temporal = new File("temp.csv");
        
        try {
            FileWriter filewriter = new FileWriter(archivo_temporal, true);
            FileReader filereader = new FileReader(archivo_padrino);
            BufferedReader bf = new BufferedReader(filereader);
            String s;
            s = bf.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String idpersona_archivo = st.nextToken();
                String nombre_padrino = st.nextToken();
                String telefono_padrino = st.nextToken();
                String email_padrino = st.nextToken();
                Padrino padrino = new Padrino(idpersona_archivo, nombre_padrino, telefono_padrino, email_padrino);
                if (p.getIdPersona().equals(idpersona_archivo)) {
                   filewriter.write(p.getIdPersona() + ";" + p.getNombre() + ";"+ p.getTelefono() + ";" + p.getEmail() + ";\r\n");
                } else {
                    filewriter.write(padrino.getIdPersona() + ";" + padrino.getNombre() + ";"+ padrino.getTelefono() + ";" + padrino.getEmail() + ";\r\n");
                }

                s = bf.readLine();

            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        archivo_padrino.delete();
        boolean rename = archivo_temporal.renameTo(new File("padrino.csv"));
    }
     
    @Override
    public void update(Perro p) {
        File fo = new File("temp.csv");
        
        try {
            FileWriter ft = new FileWriter(fo, true);
            FileReader fr = new FileReader(archivo_perro);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String idperro_archivo = st.nextToken();
                String nombre_perro = st.nextToken();
                String nChip_perro = st.nextToken();
                String raza_perro = st.nextToken();
                Perro perro = new Perro(idperro_archivo, nombre_perro, nChip_perro, raza_perro);
                if (p.getIdPerro().equals(idperro_archivo)) {
                   ft.write(p.getIdPerro() + ";" + p.getNombre() + ";"+ p.getNChip() + ";" + p.getRaza() + ";\r\n");
                } else {
                    ft.write(perro.getIdPerro() + ";" + perro.getNombre() + ";"+ perro.getNChip() + ";" + perro.getRaza() + ";\r\n");
                }

                s = bf.readLine();

            }
            ft.close();
            fr.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        archivo_perro.delete();
        boolean rename = fo.renameTo(new File("perro.csv"));
    } 
    
     @Override
    public void update(Apadrina apa) {
        File archivo_temporal = new File("temp.csv");
        
        try {
            FileWriter filewriter = new FileWriter(archivo_temporal, true);
            FileReader filereader = new FileReader(archivo_apadrinamiento);
            BufferedReader bf = new BufferedReader(filereader);
            String s;
            s = bf.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String idapadrinamiento = st.nextToken();
                String idpersona_archivo = st.nextToken();
                String idperro_archivo = st.nextToken();
                Date fecha = null;
                try
                {
                 fecha = SHORT_DATE_FORMAT.parse(st.nextToken());                
                }
                catch (ParseException pe)
                {                    
                }
               Apadrina apadrina = new Apadrina (idapadrinamiento, idpersona_archivo, idperro_archivo, fecha);
                if (apa.getIdApadrina().equals(idapadrinamiento)) {
                    filewriter.write(apa.getIdApadrina() + ";" + apa.getIdPersona() + ";" + apa.getIdPerro() +  ";" + SHORT_DATE_FORMAT.format(apa.getFecha()) + ";\r\n");
                } else {
                    filewriter.write(apadrina.getIdApadrina() + ";" + apadrina.getIdPersona() + ";" + apadrina.getIdPerro() + ";" + SHORT_DATE_FORMAT.format(apadrina.getFecha()) +";\r\n");
                }

                s = bf.readLine();

            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        archivo_apadrinamiento.delete();
        boolean rename = archivo_temporal.renameTo(new File("apadrinamiento.csv"));
    }
    
    @Override
     public void delete(Padrino p) {
        File fo = new File("temp.csv");
        
        try {
            FileWriter ft = new FileWriter(fo, true);
            FileReader fr = new FileReader(archivo_padrino);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String idpersona_archivo = st.nextToken();
                String nombre_padrino = st.nextToken();
                String telefono_padrino = st.nextToken();
                String email_padrino = st.nextToken();
                Padrino padrino = new Padrino(idpersona_archivo, nombre_padrino, telefono_padrino, email_padrino);
                if (!p.getIdPersona().equals(idpersona_archivo)) {
                    ft.write(padrino.getIdPersona() + ";" + padrino.getNombre() + ";"+ padrino.getTelefono() + ";" + padrino.getEmail() + ";\r\n");
                } 

                s = bf.readLine();

            }
            ft.close();
            fr.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        archivo_padrino.delete();
        boolean rename = fo.renameTo(new File("padrino.csv"));
    }
    
    @Override
    public void delete(Perro p) {
        File fo = new File("temp.csv");
        
        try {
            FileWriter ft = new FileWriter(fo, true);
            FileReader fr = new FileReader(archivo_perro);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String idperro_archivo = st.nextToken();
                String nombre_perro = st.nextToken();
                String nChip_perro = st.nextToken();
                String raza_perro = st.nextToken();
                Perro perro = new Perro(idperro_archivo, nombre_perro, nChip_perro, raza_perro);
                if (!p.getIdPerro().equals(idperro_archivo)) {
                    ft.write(perro.getIdPerro() + ";" + perro.getNombre() + ";"+ perro.getNChip() + ";" + perro.getRaza() + ";\r\n");
                } 

                s = bf.readLine();

            }
            ft.close();
            fr.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        archivo_perro.delete();
        boolean rename = fo.renameTo(new File("perro.csv"));
    }

    @Override
  public void delete(Apadrina p) {
        File fo = new File("temp.csv");
        
        try {
            FileWriter ft = new FileWriter(fo, true);
            FileReader fr = new FileReader(archivo_apadrinamiento);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
               StringTokenizer st = new StringTokenizer(s, ";");
               String idapadrinamiento = st.nextToken();
               String idpersona_archivo = st.nextToken();
               String idperro_archivo = st.nextToken();
               Date fecha = null;
                try
                {
                 fecha = SHORT_DATE_FORMAT.parse(st.nextToken());                
                }
                catch (ParseException pe)
                {                    
                }
               Apadrina apa = new Apadrina (idapadrinamiento, idpersona_archivo, idperro_archivo, fecha);
                if (!p.getIdApadrina().equals(idapadrinamiento)) {
                    ft.write(apa.getIdApadrina() + ";" + apa.getIdPersona() + ";" + apa.getIdPerro() + ";" + SHORT_DATE_FORMAT.format(apa.getFecha()) + ";\r\n");
                } 

                s = bf.readLine();

            }
            ft.close();
            fr.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        archivo_apadrinamiento.delete();
        boolean rename = fo.renameTo(new File("apadrinamiento.csv"));
    }

    @Override
    public void instalarDb() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void desinstalarDb() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}