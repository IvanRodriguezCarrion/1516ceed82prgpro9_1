package org.ceed.ceed82prgpro9.modelo;


import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ceed.ceed82prgpro9.vista.Funciones;


/**
 *
 * @author BlackBana
 */
public class ModeloDb4o implements IModelo {
  private int idpadrino = 0;
  private int idperro = 0;
  private int idapadrina = 0;
  private static final SimpleDateFormat recuperarfecha = new SimpleDateFormat("yyyy-MM-dd");
  private String error;
  private static String nomArchivo = "bdoo.db4o";
  private File archDb4O = new File(nomArchivo);
  private ObjectContainer bd;
  
  public ModeloDb4o() {
      if (!this.archDb4O.exists())
          {
            Funciones.mensaje(null, "No existe BD. Debes instalarla primero.", "Error en el archivo DB4O", 1);
          }
    }
    
  @Override
    public void instalarDb() {
        try {
        Padrino padrino1 = new Padrino ("1", "Padrino1", "96-3225445","padrino1@padrino1.es");
        Padrino padrino2 = new Padrino ("2", "Padrino2", "65805777","padrino2@padrino2.es");
        Padrino padrino3 = new Padrino ("3", "Padrino3", "668203387","padrino3@padrino3.es");
        
        Perro perro1 = new Perro("1", "Perro1","123456789012345", "Raza 1");
        Perro perro2 = new Perro("2", "Perro2","123456789012345", "Raza 2");
        Perro perro3 = new Perro("3", "Perro3","123456789012345", "Raza 3");

        Apadrina apadrina1 = new Apadrina("1", padrino1, perro1, recuperarfecha.parse("2016-12-05"));
        Apadrina apadrina2 = new Apadrina("2", padrino2, perro2, recuperarfecha.parse("2016-05-07"));
        Apadrina apadrina3 = new Apadrina("3", padrino3, perro3, recuperarfecha.parse("2016-01-01"));
        create(padrino1);
        create(padrino2);
        create(padrino3);

        create(perro1);
        create(perro2);
        create(perro3);

        create(apadrina1);
        create(apadrina2);
        create(apadrina3);
        
      } catch (ParseException ex) {
          Funciones.mensaje(null, "Error en la instalación de la base de datos", "Error en la BDOO", 1);
      }
                 
    }
    
    public void desinstalarDb() {
        archDb4O.delete();
        System.out.println("Archivo archDb4O borrado");
    }

    
    
    public void conectarBd() {
        System.out.println("DB4O conectada");
        this.bd = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), nomArchivo);
    }
    
    public void desconectarBd() {   
        System.out.println("Connexión cerrada");
        this.bd.close();
    }
    
    @Override
    public void create(Padrino padrino) {
        idpadrino = calcularIdPad();
        conectarBd();
        padrino.setId(Integer.toString(idpadrino));
        idpadrino += 1;
        bd.store(padrino);
        desconectarBd();  
    }

    @Override
    public ArrayList<Padrino> readPadrino() {
        ArrayList<Padrino> padrinos = new ArrayList();
        Padrino padrino_tipo = new Padrino (null, null, null, null); 
        Padrino pad_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(padrino_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              pad_bdo = (Padrino)res.next();
              padrinos.add(pad_bdo);
            }
            desconectarBd();
        return padrinos;
    }

    @Override
    public void update(Padrino padrino) {
        Padrino padrino_tipo = new Padrino (padrino.getIdPersona(), null, null, null); 
        Padrino pad_actualizar = null;
        conectarBd();

        ObjectSet res = this.bd.queryByExample(padrino_tipo); // El motor de búsqueda, en este caso, buscará unicamente los objetos que tengan el parámetro que le hemos definido (pasando del resto pues son null)
        if (res.hasNext())
            {
              pad_actualizar = (Padrino)res.next();
              pad_actualizar.setNombre(padrino.getNombre());
              pad_actualizar.setEmail(padrino.getEmail());
              pad_actualizar.setTelefono(padrino.getTelefono());
              bd.store(pad_actualizar);
            }
            desconectarBd();
  
        
    }

    @Override
    public void delete(Padrino padrino) {
        Padrino padrino_tipo = new Padrino (padrino.getIdPersona(), null, null, null); 
        Padrino pad_borrar = null;
        conectarBd();

        ObjectSet res = this.bd.queryByExample(padrino_tipo); // El motor de búsqueda, en este caso, buscará unicamente los objetos que tengan el parámetro que le hemos definido (pasando del resto pues son null)
        if (res.hasNext())
            {
              pad_borrar = (Padrino)res.next();
              bd.delete(pad_borrar);
            }
            desconectarBd();
  
    }

    @Override
    public void create(Perro perro) {
        idperro = calcularIdPer();
        conectarBd();
        perro.setIdPerro(Integer.toString(idperro));
        idperro += 1;
        bd.store(perro);
        desconectarBd();       
    }

    @Override
    public ArrayList<Perro> readPerro() {
        ArrayList<Perro> perros = new ArrayList();
        Perro perro_tipo = new Perro (null, null, null, null); 
        Perro per_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(perro_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              per_bdo = (Perro)res.next();
              perros.add(per_bdo);
            }
            desconectarBd();
        return perros;
    }

    @Override
    public void update(Perro perro) {
        Perro perro_tipo = new Perro (perro.getIdPerro(), null, null, null); 
        Perro per_actualizar = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(perro_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              per_actualizar = (Perro)res.next();
              per_actualizar.setNombre(perro.getNombre());
              per_actualizar.setNChip(perro.getNChip());
              per_actualizar.setRaza(perro.getRaza());
            }
            desconectarBd();     
    }

    @Override
    public void delete(Perro perro) {
        Perro perro_tipo = new Perro (perro.getIdPerro(), null, null, null); 
        Perro per_borrar = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(perro_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              per_borrar = (Perro)res.next();
              bd.delete(per_borrar);
            }
            desconectarBd();     
    }

    @Override
    public void create(Apadrina apadrina) {
        idapadrina = calcularIdApa();
        conectarBd();
        apadrina.setIdApadrina(Integer.toString(idapadrina));
        Padrino pa = recuperarPadrino(apadrina);
        Perro pe = recuperarPerro(apadrina);
        apadrina.setPadrino(pa);
        apadrina.setPerro(pe);
        idapadrina += 1;
        bd.store(apadrina);
        desconectarBd();
    }

    @Override
    public ArrayList<Apadrina> readApadrina() {
        ArrayList<Apadrina> apadrinas = new ArrayList();
        Perro perramen = null;
        Apadrina apadrina_tipo = new Apadrina (null, null, perramen, null); 
        Apadrina apa_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(apadrina_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              apa_bdo = (Apadrina)res.next();
              apadrinas.add(apa_bdo);
            }
            desconectarBd();
        return apadrinas;    }

    @Override
    public void update(Apadrina apadrina) {
     Perro perramen = null;
     Apadrina apadrina_tipo = new Apadrina (apadrina.getIdApadrina(), null, perramen, null); 
     Apadrina apa_actualizar = null;

     conectarBd();

     ObjectSet res = this.bd.queryByExample(apadrina_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              apa_actualizar = (Apadrina)res.next();
              apa_actualizar.setPadrino(apadrina.getPadrino());
              apa_actualizar.setPerro(apadrina.getPerro());
              apa_actualizar.setFecha(apadrina.getFecha());
            }
            desconectarBd();
    }

    @Override
    public void delete(Apadrina apadrina) {
        Perro perramen = null;
        Apadrina apadrina_tipo = new Apadrina (null, null, perramen, null); 
        Apadrina apa_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(apadrina_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              apa_bdo = (Apadrina)res.next();
              bd.delete(apa_bdo);
            }
            desconectarBd();
    }
    
    
    public int calcularIdPad() {
        int idPadSig = 0;
        // padrino_tipo lo que le pasaremos al comparador para que busque en el archivo todos los objetos del mismo tipo.
        Padrino padrino_tipo = new Padrino (null, null, null, null); 
        Padrino pad_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(padrino_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              pad_bdo = (Padrino)res.next();
              String id = pad_bdo.getIdPersona();
              if (Integer.parseInt(id) > idPadSig) {
                idPadSig = Integer.parseInt(id);
              }
              //Este IF está justificado pensando en que desconocemos la ordenación que va a hacer el motor de búsqueda a la hora de ordenar los objetos. 
              //Si los ordena de forma aleatoria y no hiciesemos un if controlando la ID, cogería la última que almacenase y podría ser que no fuese la más alta.
            }
            desconectarBd();
        return idPadSig + 1;
    }
    
    public int calcularIdPer() {
        int idPerSig = 0;
        // perro_tipo lo que le pasaremos al comparador para que busque en el archivo todos los objetos del mismo tipo.
        Perro perro_tipo = new Perro (null, null, null, null); 
        Perro per_bdo = null;

        conectarBd();

        ObjectSet res = bd.queryByExample(perro_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              per_bdo = (Perro)res.next();
              String id = per_bdo.getIdPerro();
              if (Integer.parseInt(id) > idPerSig) {
                idPerSig = Integer.parseInt(id);
              }
              //Este IF está justificado pensando en que desconocemos la ordenación que va a hacer el motor de búsqueda a la hora de ordenar los objetos. 
              //Si los ordena de forma aleatoria y no hiciesemos un if controlando la ID, cogería la última que almacenase y podría ser que no fuese la más alta.
            }
            desconectarBd();
        return idPerSig + 1;
    }
    
    public int calcularIdApa() {
        int idApaSig = 0;
        // perro_tipo lo que le pasaremos al comparador para que busque en el archivo todos los objetos del mismo tipo.
        Perro perramen = null;
        Apadrina apa_tipo = new Apadrina (null, null, perramen, null); 
        Apadrina apa_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(apa_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              apa_bdo = (Apadrina)res.next();
              String id = apa_bdo.getIdApadrina();
              if (Integer.parseInt(id) > idApaSig) {
                idApaSig = Integer.parseInt(id);
              }
              //Este IF está justificado pensando en que desconocemos la ordenación que va a hacer el motor de búsqueda a la hora de ordenar los objetos. 
              //Si los ordena de forma aleatoria y no hiciesemos un if controlando la ID, cogería la última que almacenase y podría ser que no fuese la más alta.
            }
            desconectarBd();
        return idApaSig + 1;
    }
    // Con estos métodos nos aseguramos de que el objeto que recuperamos es el MISMO que tenemos en la BDOO y no uno nuevo, lo que nos produciría duplicidades en el registro.
    
    public Padrino recuperarPadrino(Apadrina apadrina) {
        Padrino padrino_tipo = new Padrino (apadrina.getPadrino().getIdPersona(), null, null, null); 
        Padrino pad_recuperar = null;
        ObjectSet res = bd.queryByExample(padrino_tipo);
        pad_recuperar = (Padrino)res.next();
        return pad_recuperar;
    }
    
    public Perro recuperarPerro(Apadrina apadrina) {
        Perro perro_tipo = new Perro (apadrina.getPerro().getIdPerro(), null, null, null); 
        Perro per_recuperar = null;
        ObjectSet res = bd.queryByExample(perro_tipo);
        per_recuperar = (Perro)res.next();
        return per_recuperar;
    }
    
    public Padrino recuPadId(String id) {
        Padrino padrino_tipo = new Padrino (id, null, null, null); 
        Padrino pad_recuperar = null;
        ObjectSet res = bd.queryByExample(padrino_tipo);
        pad_recuperar = (Padrino)res.next();
        return pad_recuperar;
        
    }
    
    public Perro recuPerId(String id) {
        Perro perro_tipo = new Perro (id, null, null, null); 
        Perro per_recuperar = null;
        ObjectSet res = bd.queryByExample(perro_tipo);
        per_recuperar = (Perro)res.next();
        return per_recuperar;
        
    }
    
}
