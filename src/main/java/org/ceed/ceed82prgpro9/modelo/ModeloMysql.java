package org.ceed.ceed82prgpro9.modelo;

import java.sql.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import javax.swing.JOptionPane;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.PooledConnection;
import java.text.SimpleDateFormat;
import org.ceed.ceed82prgpro9.vista.Funciones;


/**
 *
 * @author BlackBana
 */
public class ModeloMysql implements IModelo {
    private final String bbdd = "1516ceedprg";
    private final String usuario = "alumno";
    private final String password = "alumno";
    private static final SimpleDateFormat recuperarfecha = new SimpleDateFormat("yyyy-MM-dd");
    private final String jdbcUrl = "jdbc:mysql://localhost/" + bbdd;
    private MysqlConnectionPoolDataSource mcpds = new MysqlConnectionPoolDataSource();
    private PooledConnection pc = null;
    private Connection cn = null;
    private Statement st = null;
    private ResultSet rs = null;
    private String error, sql, valor;
    
    
    
  public ModeloMysql() {
      mcpds.setUser(usuario);
      mcpds.setPassword(password);
      mcpds.setUrl(jdbcUrl);
    }
  
    
    public String crearTablas() {
        error = null;
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            
            sql = "DROP TABLE IF EXISTS apadrina;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "DROP TABLE IF EXISTS padrinos;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "DROP TABLE IF EXISTS perros;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "CREATE TABLE IF NOT EXISTS `padrinos` (\n"
               + "  `idpersona` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "  `nombrepersona` varchar(20),\n"
               + "  `telefono` varchar(15),\n"
               + "  `email` varchar(50),\n"
               + "  PRIMARY KEY (`idpersona`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0\n ;";
            System.out.println(sql);
            st.executeUpdate(sql);


            sql = "CREATE TABLE IF NOT EXISTS `perros` (\n"
               + "  `idperro` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "  `nombreperro` varchar(20),\n"
               + "  `nchip` BIGINT(15),\n"
               + "  `raza` varchar(15),\n"
               + "  PRIMARY KEY (`idperro`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0\n ;";
            System.out.println(sql);
            st.executeUpdate(sql);

            
            sql = "CREATE TABLE IF NOT EXISTS `apadrina` (\n"
               + "  `idapadrina` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "  `idpadrino_apa` int(5) NOT NULL,\n"
               + "  `idperro_apa` int(5) NOT NULL,\n"
               + "  `fechaapa` date NOT NULL,\n"
               + "  PRIMARY KEY (`idapadrina`),\n"    
               + "  FOREIGN KEY (`idpadrino_apa`) REFERENCES padrinos(idpersona),\n"
               + "  FOREIGN KEY (`idperro_apa`) REFERENCES perros(idperro)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0\n ;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            Funciones.mensaje(null, "Tablas creadas con éxito.", "CREACION DE TABLAS",1);
            st.close();
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
            Funciones.mensaje(null, "Fallo en la creación de las tablas.", "CREACION DE TABLAS",0);
        }
        desconectarBd();
        return error;
    }
    
    public String insertarDatos() {
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            
            sql = "INSERT INTO padrinos(nombrepersona,telefono,email) VALUES('Padrino1','1111111', 'padrino1@padrinos.es');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
			
            sql = "INSERT INTO padrinos(nombrepersona,telefono,email) VALUES('Padrino2','2222222', 'padrino2@padrinos.es');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO padrinos(nombrepersona,telefono,email) VALUES('Padrino3','33333333', 'padrino3@padrinos.es');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO perros(nombreperro,nchip,raza) VALUES('Perro1', 123456789012345,'mestizo');\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "INSERT INTO perros(nombreperro,nchip,raza) VALUES('Perro2', 123456789012345,'Raza1');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO perros(nombreperro,nchip,raza) VALUES('Perro3', 123456789012345,'Raza2');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO apadrina (idpadrino_apa,idperro_apa,fechaapa) VALUES(1, 1,'2016/05/12');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO apadrina (idpadrino_apa,idperro_apa,fechaapa) VALUES(2, 2,'2016/01/17');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            Funciones.mensaje(null, "Datos genéricos insertados con éxito.", "INSERCION DE DATOS",1);
            
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
            Funciones.mensaje(null, "Fallo en la inserción de datos genéricos.", "INSERCION DE DATOS",0);
        }
        desconectarBd();
        return error;
            
    }
	
	public void conectarBd() {
        try {
            pc = mcpds.getPooledConnection();
            cn = pc.getConnection();
            System.out.println("Base de datos " + bbdd + ": conectada");
        } catch (SQLException se) {
            se.printStackTrace();
            Funciones.mensaje(null, "Fallo al conectar con la base de datos.", "CONEXION CON BASE DE DATOS",0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void desconectarBd() {
        try {
            System.out.println("Base de datos " + bbdd + ": desconectada");
            cn.close();
        } catch (SQLException se) {
            se.printStackTrace();
            Funciones.mensaje(null, "Fallo al desconectar de la base de datos.", "CONEXION CON BASE DE DATOS",0);
        }
    }

    
    @Override
    public void create(Padrino padrino) {
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "INSERT INTO padrinos (nombrepersona, telefono, email) VALUES ('" + padrino.getNombre() + "', '" + padrino.getTelefono() + "', '" + padrino.getEmail() + "');";
            System.out.println(sql);
            int resultado = this.st.executeUpdate(sql);
             
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al insertar los valores.", "INSERT",0);
        }
        
    }

    @Override
    public ArrayList<Padrino> readPadrino() {
        ArrayList<Padrino> padrinos = new ArrayList();
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "SELECT idpersona, nombrepersona, telefono, email\n FROM padrinos\n ORDER BY idpersona;";
            System.out.println(sql);
            rs = st.executeQuery(sql);
            while (rs.next()) {
               Padrino padrino = new Padrino();
               
               padrino.setIdPersona(rs.getString("idpersona"));
               padrino.setNombre(rs.getString("nombrepersona"));
               padrino.setTelefono(rs.getString("telefono"));
               padrino.setEmail(rs.getString("email"));
               
               padrinos.add(padrino);
            }
        } catch (SQLException se) {
          se.printStackTrace();
          Funciones.mensaje(null, "Fallo al leer en la base de datos", "READ",0);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        desconectarBd();
        return padrinos;
    }

    @Override
    public void update(Padrino padrino) {
           try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "UPDATE padrinos SET nombrepersona = '" + padrino.getNombre() + "', telefono = '" 
                    + padrino.getTelefono() + "', email = '" + padrino.getEmail() + "' WHERE idpersona = '" + padrino.getIdPersona() + "';";
            System.out.println(sql);
            int resultado = this.st.executeUpdate(sql);
            desconectarBd();
            
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al actualizar la base de datos.", "UPDATE",0);
        }
    }

    @Override
    public void delete(Padrino padrino) {
         try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "DELETE FROM padrinos WHERE idpersona = '" + padrino.getIdPersona() + "';";
            System.out.println(sql);
            int resultado = this.st.executeUpdate(sql);
            desconectarBd();
            
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al eliminar en la base de datos.", "DELETE",0);
        }
    }

    @Override
    public void create(Perro perro) {
            try {
                conectarBd();
                st = (Statement) cn.createStatement();
                sql = "INSERT INTO perros (nombreperro, nchip, raza) VALUES ('" + perro.getNombre() + "', '" + perro.getNChip() + "', '" + perro.getRaza() + "');";
                System.out.println(sql);
                int resultado = this.st.executeUpdate(sql);
             
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al insertar los valores.", "INSERT",0);

        }
    }

    @Override
    public ArrayList<Perro> readPerro() {
        ArrayList<Perro> perros = new ArrayList();
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "SELECT idperro, nombreperro, nchip, raza FROM perros ORDER BY idperro;";
            System.out.println(sql);
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Perro perro = new Perro();
                perro.setIdPerro(rs.getString("idperro"));
                perro.setNombre(rs.getString("nombreperro"));
                perro.setNChip(rs.getString("nchip"));
                perro.setRaza(rs.getString("raza"));
                
                perros.add(perro);
            }
        } catch (SQLException se) {
          se.printStackTrace();
          Funciones.mensaje(null, "Fallo al leer en la base de datos", "READ",0);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        desconectarBd();
        return perros;
    }

    @Override
    public void update(Perro perro) {
           try {
                conectarBd();
                st = (Statement) cn.createStatement();
                sql = "UPDATE perros SET nombreperro = '" + perro.getNombre() + "', nchip = '" 
                        + perro.getNChip() + "', raza = '" + perro.getRaza() + "' WHERE idperro = '" + perro.getIdPerro() + "';";
                System.out.println(sql);
                int resultado = this.st.executeUpdate(sql);
                desconectarBd();
            
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al actualizar la base de datos.", "UPDATE",0);
        }
    }

    @Override
    public void delete(Perro perro) {
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "DELETE FROM perros WHERE idperro = '" + perro.getIdPerro() + "';";
            System.out.println(sql);
            int resultado = this.st.executeUpdate(sql);
            desconectarBd();
            
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al eliminar en la base de datos.", "DELETE",0);
        }    }

    @Override
    public void create(Apadrina apadrina) {
          try {
                conectarBd();   
                st = (Statement) cn.createStatement();
                sql = "INSERT INTO apadrina (idpadrino_apa, idperro_apa, fechaapa) VALUES ('" + apadrina.getIdPersona() + "', '" + apadrina.getIdPerro() + "', '" + recuperarfecha.format(apadrina.getFecha()) + "');";
                System.out.println(sql);
                int resultado = this.st.executeUpdate(sql);
             
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al insertar los valores.", "INSERT",0);
        }    }

    @Override
    public ArrayList<Apadrina> readApadrina() {
        ArrayList<Apadrina> apadrinas = new ArrayList();
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "SELECT idapadrina, idpadrino_apa, idperro_apa, fechaapa FROM apadrina ORDER BY idapadrina;";
            System.out.println(sql);
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Apadrina apadrina = new Apadrina();
                apadrina.setIdApadrina(rs.getString("idapadrina"));
                apadrina.setIdPersona(rs.getString("idpadrino_apa"));
                apadrina.setIdPerro(rs.getString("idperro_apa"));
                apadrina.setFecha(recuperarfecha.parse(rs.getString("fechaapa")));
                
                apadrinas.add(apadrina);
            }
        } catch (SQLException se) {
          se.printStackTrace();
          Funciones.mensaje(null, "Fallo al leer en la base de datos", "READ",0);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        desconectarBd();
        return apadrinas;    }

    @Override
    public void update(Apadrina apadrina) {
        try {
                conectarBd();
                st = (Statement) cn.createStatement();
                sql = "UPDATE apadrina SET idpadrino_apa = '" 
                        + apadrina.getIdPersona() + "', idperro_apa = '" + apadrina.getIdPerro() + "', fechaapa = '" + recuperarfecha.format(apadrina.getFecha()) + "' WHERE idapadrina = '" + apadrina.getIdApadrina() + "';";
                System.out.println(sql);
                int resultado = this.st.executeUpdate(sql);
                desconectarBd();
            
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al actualizar la base de datos.", "UPDATE",0);
        }  
    }

    @Override
    public void delete(Apadrina apadrina) {
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "DELETE FROM apadrina WHERE idapadrina = '" + apadrina.getIdApadrina() + "';";
            System.out.println(sql);
            int resultado = st.executeUpdate(sql);
            desconectarBd();
            
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al eliminar en la base de datos.", "DELETE",0);
        }
    }

    @Override
    public void instalarDb() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void desinstalarDb() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
